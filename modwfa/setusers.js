jQuery(document).ready(function($){
  
  values = Drupal.settings.modwfa.users;
  
  jQuery.fn.extend({
    propAttr: $.fn.prop || $.fn.attr
  });
  
  // $("#edit-submitted-submitter").autocomplete({ source: values });
  $("#edit-submitted-submitter").autocomplete({ source: values, minLength: 0 }).focus(function() { $(this).autocomplete('search'), $(this).val() });
  $("#edit-submit").click(function() {
    if (values.indexOf($('#edit-submitted-submitter').val()) <= -1) {
      $('#edit-submitted-submitter').focus();
      alert('請點選人員');
      return false;
    } else {
      return true;
    }
  });
});  