jQuery(document).ready(function($){

function _get1st(str, l) {
  pos = str.indexOf(l);
  return str.substring(0, pos);
}

function _get2nd(str, l) {
  pos = str.indexOf(l);
  return str.substring(pos+1);
}
 
function noDupUser() {
  $('#nlist2 option').removeAttr("disabled");
  $('#nlist1 option').each(function(index) {
    // console.log($(this).attr('value'));
    $('#nlist2 option[value="'+$(this).attr('value') +'"]').attr("disabled", "disabled");
  });
}

$("#addWebformUser").click(function() {
  $("#nlist2 option:selected").each(function(index) {
     $("#nlist1").append($(this).clone());
     $(this).removeAttr("selected"); 
  }); 
  noDupUser();
}); 

$("#removeWebformUser").click(function() {
  $("#nlist1 option:selected").each(function(index) {
    $(this).remove(); 
  }); 
  noDupUser(); 
});  

$("#search").keyup(function() {
  search_str = $(this).val();
  // alert(search_str + select_src);
  $('#nlist2').find('option').attr('selected', '').filter(function () {
      if (search_str == '') {
        return false;
      } 
      if ($(this).attr('disabled')) {
        return false;
      }
      return $(this).text().indexOf(search_str) > - 1;
  }).attr('selected', 'selected');
});
 
$("#addToWebform").click(function(){
  userdata = []; 
  $("#nlist1 option").each(function() {
      // userdata += $(this).attr('name') + "|" + $(this).attr('value') + "|" + $(this).text() + "_";
      arr = [];
      arr.push(_get1st($(this).attr('name'), '|'));
      arr.push(_get2nd($(this).attr('name'), '|'));
      arr.push($(this).attr('value'));
      arr.push(_get2nd($(this).text(), '-'));
      userdata.push(arr);
  });
  // if (userdata.length > 0 ) {
    var jsonString = JSON.stringify(userdata);
    $("#ajaxloader").show();
    $.ajax({
      url: this.href, 
      cache: false,
      dataType: 'json',
      type:'POST',
      data: { userdata: jsonString, action: 'add_users'},
      error: function(xhr) {
        alert('Ajax request 發生錯誤');
        $("#ajaxloader").hide();
        /* $("#nowloading").hide(); */
      },
      success: function(response) {
        // alert('Ajax request success');
        // console.log(response);
        alert('名單加入完成');
        $("#ajaxloader").hide();
      }
    });
  // } else {
  //  alert('請先選名單');
  // }
});
 
$("#branch").change(function(){
  
  if ($(this).attr('value').length > 0 ) {
    $("#ajaxloader").show();
    $.ajax({
      url: this.href, 
      cache: false,
      dataType: 'json',
      type:'POST',
      data: { branch: this.value, action: 'branch_change'},
      error: function(xhr) {
        alert('Ajax request 發生錯誤');
        $("#ajaxloader").hide();
        /* $("#nowloading").hide(); */
      },
      success: function(response) {
        // alert('Ajax request success');
        // console.log(response);
        $("#department").empty();
        $("#department").append('<option value="" selected="selected">選擇</option>');
        $("#department").append(response.department); 
        $("#ajaxloader").hide();
        
      }
    });
  } else {
    $("#department").empty();
  }
});

$("#department").change(function(){

  if ($(this).attr('value').length > 0) {
    $("#ajaxloader").show();
    $.ajax({
      url: this.href, 
      cache: false,
      dataType: 'json',
      type:'POST',
      data: { branch: $('#branch option:selected').attr('value'), dept: this.value, action: 'dept_change'},
      error: function(xhr) {
        alert('Ajax request 發生錯誤');
        $("#ajaxloader").hide();
        /* $("#nowloading").hide(); */
      },
      success: function(response) {
        // alert('Ajax request success');
        // console.log(response);
        $("#nlist2").empty();
        $("#nlist2").append(response.user); 
        $("#ajaxloader").hide();
        noDupUser();
        
      }
    });
  } else {
    $("#nlist2").empty();
  } 
});

});
